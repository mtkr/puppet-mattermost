define mattermost::plugin (
  $plugin_name = $title,
  $source,
  $install_dir = "${mattermost::install::dir}/plugins"
) {
  include mattermost

  ensure_resource(
    'file', "${mattermost::install::dir}/plugins", {
      'ensure' => 'directory',
      'owner'  => $mattermost::user ,
      'group'  => $mattermost::group,
      'mode'   => '0755'
    }
  )

  archive { $plugin_name:
    path         => "/tmp/${plugin_name}.tar.gz",
    source       => $source,
    extract      => true,
    extract_path => $install_dir,
    cleanup      => true,
    creates      => "${install_dir}/${plugin_name}/plugin.json",
    user         => $mattermost::user,
    group        => $mattermost::group,
    require      => Class['mattermost::config'],
  }
}
